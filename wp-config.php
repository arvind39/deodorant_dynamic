<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'deodorant' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'mx&io6SX*O2$pa$ybX^^PnOC`TZp1]^]HL?+Uz~J1LXu+]_,>fZZ5^ByaSLq4gkO' );
define( 'SECURE_AUTH_KEY',  'pNS{8b[o[kxDyIT6{;hO$RUH;k3/+X^t8QM$TLg/b,^oxl%pga<m7yt$z&_H+k8g' );
define( 'LOGGED_IN_KEY',    'bW:1*k#]g?pRn%3d)YHb<P2QB{N`q<z@IDlyNe-k<`5DA)F*NrrdWEH9w/sXB-5>' );
define( 'NONCE_KEY',        'b/ PQzxKYU2&@/3SC{},8Ehgl Q8 v5Z#^{7H?S wtCiCmy@&EJy,Ii8EAI8o[]@' );
define( 'AUTH_SALT',        'KsU/IMXm}fD^G|K[xm-b/IlU-d|`+Tz_h@6Uf,vqW$ |z_u 8mOe50.:e=$PuWaW' );
define( 'SECURE_AUTH_SALT', 'UQ}<3#jp,,2z0Y9PyiMWuSN&Mw)#z2M(SRYqF 58} ryuL)w&U~w|xlw5)tD9bFn' );
define( 'LOGGED_IN_SALT',   '5s_(L+30aFqak=&S3i^eg6dfzuK:Mb{U7fs{ro4m4%P{L.o.(NiXT i$laN<+y;A' );
define( 'NONCE_SALT',       'G,-y}97UQ-N@(ex[W6oU}4&LwbS:h/si1POOYxQk9/++YQU10o*rn1o:m H98<Pb' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
