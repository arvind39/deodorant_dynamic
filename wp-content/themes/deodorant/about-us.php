<?php

/**
/* Template Name: About Us Page
 *
 * Displays Only about template
 
 * @package WordPress
 * @subpackage deodorant
 * @since deodorant 1.0
 */
get_header(); ?>

<?php

$post1 = get_post(29);
$post1_title = $post1->post_title;
$post1_text = $post1->post_content;
$post1_img = get_the_post_thumbnail_url($post1, 'thumbnail');




?>


<?php $image_url = wp_get_attachment_url(get_post_thumbnail_id()); ?>



<?php if (!empty(get_the_post_thumbnail())) { ?>

  <section class="page_banner" style="background-image:url('<?php echo $image_url; ?>"></section>
<?php } else { ?>
  <section class="page_banner" style="background-image:url('<?php echo esc_url(get_template_directory_uri()); ?>/images/images-about-us-1.jpg');">

  </section>
<?php } ?>

<!--********** -->


<section class="about-us-sec1 py-5 my-5">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-12">
        <div class="about_text ">
          <!-- <h2>Our Mission</h2> -->
          <h2><?php echo $post1_title ?></h2>
          <div class="mt-4 pt-4 border_bottom">
          <?php echo $post1_text ?>
            <!-- <li>DEODORANT DAN is the first website devoted to you and making sure you always smell amazing.</li>
            <li>We’re here to make sure that you have your deodorant as frequently as you desire and with ease.</li>
            <li>Take the hassle out of buying your deodorant while you save big as it is easily shipped directly to your front door.</li>
            <li>If you smell amazing so should your wife, girlfriend, and your children, right? That’s why Deodorant Dan is the first site to offer deodorant exclusively for both men and woman.</li>
            <li>We strive so that while you look great…you also smell amazing!</li> -->
</div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>