<?php

/**
/* Template Name: Cart Page
 *
 * Displays Only about template
 
 * @package WordPress
 * @subpackage deodorant
 * @since deodorant 1.0
 */
get_header(); ?>

<!--Banner Part-->

<?php $image_url = wp_get_attachment_url(get_post_thumbnail_id()); ?>
<?php if (!empty(get_the_post_thumbnail())) { ?>

	<section class="page_banner aaa" style="background-image:url('<?php echo $image_url; ?>"></section>
<?php } else { ?>
	<section class="page_banner default_banner" style="background-image:url('<?php echo esc_url(get_template_directory_uri()); ?>/images/images-about-us-1.jpg');">

	</section>
<?php } ?>


<!-- -->
<section class="cart_page py-5 my-2">
	<div class="container">
		<div class="row align-items-center justify-content-center">
			<div class="col-md-12">
				<div class="cart_list">
					<div class="page_title">
						<h2>Cart</h2>
					</div>
					<?php if (have_posts()) : ?>
						<div>
							<?php while (have_posts()) : the_post(); ?>
								<?php the_content(); ?>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>



<?php get_footer(); ?>