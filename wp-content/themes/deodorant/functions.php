<?php
/**
 * deodorant functions and definitions
 *
 * @package WordPress
 * @subpackage deodorant
 * @since deodorant 1.0
 */
/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since deodorant 1.0
 */
 load_theme_textdomain( 'deodorant' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'deodorant' ),
		'header'  => __( 'Header menu', 'deodorant' ),
		'footer'  => __( 'Footer menu', 'deodorant' ),
	) );
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
	) );
	add_theme_support( 'custom-logo', array(
		'height'      => 248,
		'width'       => 248,
		'flex-height' => true,
	) );

	add_theme_support( 'customize-selective-refresh-widgets' );
	/*
 * Loads the Options Panel
 *
 * If you're loading from a child theme use stylesheet_directory
 * instead of template_directory
 */

define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
require_once dirname( __FILE__ ) . '/inc/options-framework.php';

// Loads options.php from child or parent theme
$optionsfile = locate_template( 'options.php' );
load_template( $optionsfile );

/*
 * This is an example of how to add custom scripts to the options panel.
 * This one shows/hides the an option when a checkbox is clicked.
 *
 * You can delete it if you not using that option
 */
 
add_action( 'optionsframework_custom_scripts', 'optionsframework_custom_scripts' );

function optionsframework_custom_scripts() { ?>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#example_showhidden').click(function() {
  		jQuery('#section-example_text_hidden').fadeToggle(400);
	});

	if (jQuery('#example_showhidden:checked').val() !== undefined) {
		jQuery('#section-example_text_hidden').show();
	}

});
</script>
<?php
}
/*
* Creating a function to create widget
*/
function wpdocs_theme_slug_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Terms of use and Privacy Policy', 'deodorant' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'deodorant' ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ) );
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets_init' );

/*

*/
function wpdocs_theme_slug_widgets2_init() {
	register_sidebar( array(
			'name'          => __( 'Phone Number', 'deodorant' ),
			'id'            => 'sidebar-2',
			'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'deodorant' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '',
			'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets2_init' );

/*

*/

function wpdocs_theme_slug_widgets3_init() {
	register_sidebar( array(
			'name'          => __( 'Email Address', 'deodorant' ),
			'id'            => 'sidebar-3',
			'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'deodorant' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '',
			'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets3_init' );



function wpdocs_theme_slug_widgets4_init() {
	register_sidebar( array(
			'name'          => __( 'Social Media', 'deodorant' ),
			'id'            => 'sidebar-4',
			'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'deodorant' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '',
			'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets4_init' );


function wpdocs_theme_slug_widgets5_init() {
	register_sidebar( array(
			'name'          => __( 'search', 'deodorant' ),
			'id'            => 'sidebar-5',
			'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'deodorant' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '',
			'after_title'   => '',
	) );
}
add_action( 'widgets_init', 'wpdocs_theme_slug_widgets5_init' );

/* Creating a function to create our slider
*/

function slider_post_type() {
	$labels = array(
		'name'                => _x( 'Slider', 'Post Type General Name', 'deodorant' ),
		'singular_name'       => _x( 'slider', 'Post Type Singular Name', 'deodorant' ),
		'menu_name'           => __( 'Home Slider', 'deodorant' ),
		'parent_item_colon'   => __( 'Parent slider', 'deodorant' ),
		'all_items'           => __( 'All slider', 'deodorant' ),
		'view_item'           => __( 'View slider', 'deodorant' ),
		'add_new_item'        => __( 'Add New slider', 'deodorant' ),
		'add_new'             => __( 'Add New', 'deodorant' ),
		'edit_item'           => __( 'Edit slider', 'deodorant' ),
		'update_item'         => __( 'Update slider', 'deodorant' ),
		'search_items'        => __( 'Search slider', 'deodorant' ),
		'not_found'           => __( 'Not Found', 'deodorant' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'deodorant' ),
	);
	$args = array(
		'label'               => __( 'Slider', 'deodorant' ),
		'description'         => __( 'Slider news and reviews', 'deodorant' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', ),
		'taxonomies'          => array( 'genres' ),	
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,

		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
		'taxonomies'          => array( 'category' ),
	);
	register_post_type( 'slider', $args );
}
add_action( 'init', 'slider_post_type', 0 );

// Register Custom Navigation Walker
require_once('wp-bootstrap-navwalker.php');
/**
 * Enables the Excerpt meta box in Page edit screen.
 */
function add_excerpt_support_for_pages() {
	add_post_type_support( 'page', 'excerpt' );
}
add_action( 'init', 'add_excerpt_support_for_pages' );
/**
 * Add Subtitle in admin post
 *
 * @param WP_Post $post Post object.
 *
 * @return void
 */
function subtitle( $post ) {

	if ( ! in_array( $post->post_type, [ 'post', 'page' ], true ) ) {
		return;
	}

	// The subtitle field.
	$_stitle = sanitize_text_field( get_post_meta( $post->ID, '_subtitle', true ) );
	
    echo '<div class="inside">';
	echo '<div id="edit-slug-box" class="hide-if-no-js">';
	echo '<label for="subtitle"><strong>' . __( 'Sub Title: ' ) . '</strong></label>';
    echo '<input type="text" name="subtitle" id="subtitle" value="' .  $_stitle . '" size="30" spellcheck="true" autocomplete="off" />';
	echo '</div>';
    echo '</div>';	
}





/**
 * Save Subtitle
 *
 * @param int     $post_ID Post ID.
 * @param WP_Post $post    Post object.
 * @param bool    $update  Whether this is an existing post being updated or not.
 *
 * @return void
 */
function save_subtitle( $post_ID, $post, $update ) {

	if ( ! in_array( $post->post_type, [ 'post', 'page' ], true ) ) {
		return;
	}

	// Prevent to execute twice.
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		return;
	}

    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }

	// Get the subtitle value from $_POST.
	$_stitle = filter_input( INPUT_POST, 'subtitle', FILTER_SANITIZE_STRING );

	if ( $update ) {
		// Update the post meta.
		update_post_meta( $post_ID, '_subtitle', sanitize_text_field( $_stitle ) );
	} else if ( ! empty ( $_stitle ) ) {
		// Add unique post meta.
		add_post_meta( $post_ID, '_subtitle', sanitize_text_field( $_stitle ), true );
	}
}

add_action( 'edit_form_after_title', 'subtitle', 20 );
add_action( 'wp_insert_post', 'save_subtitle', 20, 3 );



add_filter('wpcf7_autop_or_not', '__return_false');
/*Contact form 7 remove span*/
add_filter('wpcf7_form_elements', function($content) {
	$content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

	$content = str_replace('<br />', '', $content);
			
	return $content;
});


//Only show products in the front-end search results
function lw_search_filter_pages($query) {
	if ($query->is_search) {
	$query->set('post_type', 'product');
	$query->set( 'wc_query', 'product_query' );
	}
	return $query;
	}
	 
	add_filter('pre_get_posts','lw_search_filter_pages');