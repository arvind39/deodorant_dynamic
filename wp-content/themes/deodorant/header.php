<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="user-scalable=no, width=device-width" />
  <title>deodorant</title>
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo esc_url(get_template_directory_uri()); ?>/images/favicon.png" />
  <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/owl.carousel.min.css" rel="stylesheet">
  <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/animate.min.css" rel="stylesheet">
  <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/video-js.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo esc_url(get_template_directory_uri()); ?>/css/main.css" rel="stylesheet" type="text/css" />

</head>

<body>
  <header class="w-100">

    <div class="main_menu" id="myHeader">
      <div class="container">
        <div class="row">

          <div class="col-md-12">
            <div class="main_header">
              <nav class="navbar navbar-expand-lg p-0">
                <div class="d-flex w-100 d-flex align-items-center justify-content-between">
                  <a class="navbar-brand" href="<?php echo get_site_url(); ?>">
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/dd.png" class="img-fluid">
                  </a>
                  <div class="d-flex align-items-center flex-row-reverse flex-lg-row">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main_menu" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <div class="bar1"></div>
                      <div class="bar2"></div>
                      <div class="bar3"></div>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end p-4 p-md-0" id="main_menu">
                      <a class="navbar-brand d-lg-none d-block d-flex mb-4 p-2" href="<?php echo get_site_url(); ?>">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/dd.png" class="img-fluid">
                      </a>

                      <?php wp_nav_menu(
                        array(
                          'theme_location'    => 'primary',
                          'depth'             => 2,
                          'container'         => 'ul',
                          'container_class'   => '',
                          'container_id'      => '',
                          'menu_class'        => 'navbar-nav mb-2 mb-lg-0',
                          'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                          'walker'            => new WP_Bootstrap_Navwalker()
                        )
                      );
                      ?>

                      <!-- <ul class="navbar-nav mb-2 mb-lg-0">
                        <li class="nav-item">
                          <a class="nav-link" href="product-placement.html">Product Placement</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="influencer-marketing.html">Influencer Marketing</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="movie-subtitles.html">Movie Subtitles</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="brand-care.html">Brand Care</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="other-services.html">Other Services</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="blog.html">Blog</a>
                        </li>

                      </ul> -->

                    </div>
                    <div class="cart_part">
                      <a href="<?php echo get_site_url(); ?>/cart">
                      <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" fill="#000">
                        <path d="M505.46,111.4H159c-3.61,0-6.54,3.36-6.54,7.53s2.93,7.52,6.54,7.52H505.46c3.6,0,6.54-3.37,6.54-7.52S509.07,111.4,505.46,111.4Z" />
                        <path d="M483.62,171.75H176c-3.6,0-6.54,3.37-6.54,7.53s2.93,7.53,6.54,7.53H483.62c3.62,0,6.55-3.37,6.55-7.53S487.24,171.75,483.62,171.75Z" />
                        <path d="M460.55,232.12H189.36c-3.62,0-6.55,3.37-6.55,7.52s2.93,7.52,6.55,7.52H460.55c3.61,0,6.54-3.36,6.54-7.52S464.17,232.12,460.55,232.12Z" />
                        <polygon points="222.54 121.45 209.54 123.39 227.23 280.37 240.95 283.71 222.54 121.45" />
                        <polygon points="274.75 122.59 261.69 123.63 271.3 285.14 285.02 282.28 274.75 122.59" />
                        <polygon points="314.16 120.32 314.12 285.63 326.59 285.14 327.25 120.32 314.16 120.32" />
                        <polygon points="374.62 119.77 349.87 287.53 364.01 281.79 387.52 122.27 374.62 119.77" />
                        <polygon points="430.9 119.7 390.2 288.49 404.75 285.14 443.5 123.72 430.9 119.7" />
                        <polygon points="485.99 116.99 432.19 280.37 445.08 286.58 498.23 122.27 485.99 116.99"/>
                        <path d="M472.65,269.2H208.6L155.39,97.72c-.77,9.18-9,11.07-17,12.17l54.4,174c1.35,4.38,5,7.31,9,7.31h270.8c5.29,0,9.58-4.93,9.58-11S477.95,269.2,472.65,269.2Z" />
                        <path d="M153,89a18.07,18.07,0,0,0-12-4.44H17.12C7.66,84.56,0,91.44,0,99.93S7.66,115.3,17.12,115.3H140.94a18.81,18.81,0,0,0,2.06-.19c7.94-.86,14.13-6.55,14.9-13.72a14.15,14.15,0,0,0,.16-1.47A14.59,14.59,0,0,0,153,89Z" />
                        <path d="M197.79,351.43a38,38,0,1,0,38,38A38,38,0,0,0,197.79,351.43Zm0,66.51a28.51,28.51,0,1,1,28.5-28.5A28.5,28.5,0,0,1,197.79,417.94Z"/>
                        <path d="M394.5,351.43a38,38,0,1,0,38,38A38,38,0,0,0,394.5,351.43Zm0,66.51a28.51,28.51,0,1,1,28.5-28.5A28.5,28.5,0,0,1,394.5,417.94Z" />
                        <path d="M208,362.86l17.33-80.39-13.26-3.78-17.78,82.47a28.86,28.86,0,0,1,3.47-.23A28.4,28.4,0,0,1,208,362.86Z" />
                        <path d="M367.56,380.17H224.73a28,28,0,0,1,.81,15.71H366.75a28,28,0,0,1,.81-15.71Z" />
                      </svg>

                        <?php $cartCount = WC()->cart->get_cart_contents_count();
                        if ($cartCount != 0) {

                          echo '<span class="header-cart-count">' . $cartCount . '</span>';
                        }

                        ?>

                      </a>
                    </div>
                    <a href="#" class="search-btn">
                      <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M21.172 24l-7.387-7.387c-1.388.874-3.024 1.387-4.785 1.387-4.971 0-9-4.029-9-9s4.029-9 9-9 9 4.029 9 9c0 1.761-.514 3.398-1.387 4.785l7.387 7.387-2.828 2.828zm-12.172-8c3.859 0 7-3.14 7-7s-3.141-7-7-7-7 3.14-7 7 3.141 7 7 7z" />
                      </svg>
                    </a>

                    

                  </div>

                </div>
              </nav>
            </div>

          </div>
        </div>
      </div>
    </div>
  </header>