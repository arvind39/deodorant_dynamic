<?php

/**
/* Template Name: Home
 *
 * Displays Only Home template
 *
 * @package WordPress
 * @subpackage deodorant
 * @since deodorant 1.0
 */
get_header(); ?>

<?php

//Home Section 1

$home_post1 = get_post(25);
$home_post1_title = $home_post1->post_title;
$home_post1_text = $home_post1->post_content;
//$home_post1_img = get_the_post_thumbnail_url($home_post1, 'thumbnail');

$home_post2 = get_post(31);

$home_post2_img = get_the_post_thumbnail_url($home_post2, 'thumbnail');


$home_post3 = get_post(34);
$home_post3_img = get_the_post_thumbnail_url($home_post3, 'thumbnail');

?>

<section class="home_sec1">
  <div class="main_slider">
    <div class="owl-carousel owl-theme" id="banner_slider">


      <?php $posts = new WP_Query(array('post_type' => 'Slider', 'category_name' => 'Home Slider', 'order' => 'ASC')); ?>

      <?php while ($posts->have_posts()) : $posts->the_post(); ?>
        <div class="item">
          <img src="<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>" alt="images not found">
          <div class="cover">
            <div class="container">
              <div class="row">
                <div class="col-md-6 ">
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="header-content">
                      <h2><?php //echo get_the_content(); 
                          ?></h2>

                    </div>
                  </div>

                </div>
                <div class="col-md-4">
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endwhile; ?>
    </div>
</section>

<section class="home_sec2">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <div class="about_img">
          <div class="row">
            <div class="col-md-6">
              <div class="hover_img p-xxl-5 p-4 mx-4 mx-md-0">
                <span class="d-flex position-relative ">
                  <img src="<?php echo $home_post2_img ?>" alt="" class="img-fluid">
                </span>

              </div>
            </div>
            <div class="col-md-6 pt-sm-5">
              <div class="hover_img p-xxl-5 p-4 mt-md-5 mx-4 mx-md-0">
                <span class="d-flex position-relative pink_blue mt-5">
                  <img src="<?php echo $home_post3_img ?>" alt="" class="img-fluid">
                </span>

              </div>
            </div>
          </div>
        </div>
        <!-- <div class="left_content position-relative">
          <a href="javascript:void(0)" class="img_left img_left2 img2 abcd">  </a>

          <a href="javascript:void(0)" class="img_left img_left2 img2">  </a>
        </div> -->
      </div>
      <div class="col-md-6">
        <div class="welcome position-relative text-center mt-5 mt-md-0">
          <h2>Deodorant <span>Dan</span></h2>
          <div class="look_great pb-xl-5 pb-lg-3 ">
            <h5><?php echo $home_post1_title ?></h5>
            <!-- <h5>Look Great...Smell Amazing</h5> -->
          </div>
          <p class="px-5"><i><?php echo $home_post1_text ?></i></p>

          <a href="<?php echo get_site_url(); ?>/about" class="lnk_mr">Explore</a>
        </div>
      </div>
    </div>
  </div>







</section>



<section class="home_sec3 py-5 my-5">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="page_link d-flex align-items-center justify-content-center">
          <a href="<?php echo get_site_url(); ?>/product-category/men/" class="for_male d-flex align-items-center">

          <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/man.png" class="img-fluid ">
          <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/male-gender.png" class="img-fluid">
        
          </a>
          <a href="<?php echo get_site_url(); ?>/product-category/women/" class="for_female d-flex align-items-center">
          <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/girl.png" class="img-fluid">
          <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/woman.png" class="img-fluid">
          </a>
        </div>

      </div>
    </div>







</section>
<?php get_footer(); ?>