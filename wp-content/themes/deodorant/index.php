<?php
/**
/* Template Name: index page
 *
 * Displays Only indexpage template
 *
 * @package WordPress
 * @subpackage deodorant
 * @since deodorant 1.0
 */
get_header();	

?>
<?php $image_url = wp_get_attachment_url(get_post_thumbnail_id()); ?>



<?php if (!empty(get_the_post_thumbnail())) { ?>

  <section class="page_banner aaa" style="background-image:url('<?php echo $image_url; ?>"></section>
<?php } else { ?>
  <section class="page_banner default_banner" style="background-image:url('<?php echo esc_url(get_template_directory_uri()); ?>/images/images-about-us-1.jpg');">

  </section>
<?php } ?>
<?php if (have_posts()) : ?>
  <div>
    <?php while (have_posts()) : the_post(); ?>
    <?php the_content();?>
    <?php endwhile; ?> 
  </div>
<?php endif; ?>
<?php get_footer();?>

