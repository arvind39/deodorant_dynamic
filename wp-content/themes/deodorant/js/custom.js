
//wow animation

new WOW().init();

/***mobile menu icon****/

$(document).mouseup(function (e) {
  var container = $(".navbar-toggler");
  $('.navbar-toggler').toggleClass('change');
  // If the target of the click isn't the container
  if (!container.is(e.target) && container.has(e.target).length === 0) {
    $('.navbar-toggler').removeClass('change');
    $('.navbar-collapse').collapse('hide');
  }
});

//Banner Slider

// var myCarousel = document.querySelector('#carouselExampleFade')
// var carousel = new bootstrap.Carousel(myCarousel, {
//   interval: 10000,
//   wrap: true
// })

$(document).ready(function () {


$('#banner_slider').owlCarousel({
    items: 1, 
    loop: false,
    rewind: true,
    margin: 0,
    nav: false,
    mouseDrag: true,
    autoplay: true,
    video: true,
    lazyLoad: true,    
    autoplayTimeout: 21000,
    
    animateOut: 'slideOutUp',
    dots: true,
    dotsData: false,    
    
  });
 

  
});

$(window).scroll(function() {
  var header = $(document).scrollTop();
  var headerHeight = $("#myHeader").outerHeight();
  var firstSection =200;
  if (header > headerHeight) {
    $("#myHeader").addClass("in-view");
  } else {
    $("#myHeader").removeClass("in-view");
  }
  if (header > firstSection) {
    $("#myHeader").addClass("fixed");
  } else {
    $("#myHeader").removeClass("fixed");
  }
});
// Equal Height box

// Equal Height box
if ($(window).width() >= 768) {
  $(document).ready(function () {
    $('.products').each(function () {
      var highestBox = 0;
      $('li', this).each(function () {
        if ($(this).height() > highestBox) {
          highestBox = $(this).height();
        }
      });
      $('li', this).height(highestBox);
    });
  });
}


//*******************/
//*******************/

(function ($) {	

  $.fn.searchBox = function(ev) {
  
    var $searchEl = $('.search-elem');
    var $placeHolder = $('.placeholder');
    var $sField = $('#search-field');
  
    if ( ev === "open") {
      $searchEl.addClass('search-open')
    };
  
    if ( ev === 'close') {
      $searchEl.removeClass('search-open'),
      $placeHolder.removeClass('move-up'),
      $sField.val(''); 
    };
  
    var moveText = function() {
      $placeHolder.addClass('move-up');
    }
  
    $sField.focus(moveText);
    $placeHolder.on('click', moveText);
    
    $('.submit').prop('disabled', true);
    $('#search-field').keyup(function() {
          if($(this).val() != '') {
             $('.submit').prop('disabled', false);
          }
      });
  }	
  
  }(jQuery));
  
  $('.search-btn').on('click', function(e) {
  $(this).searchBox('open');
  e.preventDefault();
  });
  
  $('.close').on('click', function() {
  $(this).searchBox('close');
  });

