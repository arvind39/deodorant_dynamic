<?php

/**
/* Template Name: Men Product
 *
 * Displays Only about template
 
 * @package WordPress
 * @subpackage deodorant
 * @since deodorant 1.0
 */
get_header(); ?>

<?php

$post1 = get_post(15);
$post1_title = $post1->post_title;
$post1_text = $post1->post_content;
$post1_img = get_the_post_thumbnail_url($post1, 'thumbnail');




?>


<?php $image_url = wp_get_attachment_url(get_post_thumbnail_id()); ?>



<?php if (!empty(get_the_post_thumbnail())) { ?>

  <section class="page_banner" style="background-image:url('<?php echo $image_url; ?>"></section>
<?php } else { ?>
  <section class="page_banner" style="background-image:url('<?php echo esc_url(get_template_directory_uri()); ?>/images/images-about-us-1.jpg');">

  </section>
<?php } ?>

<!--********** -->


<section class="about-us-sec1 py-5 my-5">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-12">
        <div class="all_products">

          <?php echo $post1_text ?>

          <?php
          $params = array('post_type' => 'product', 'product_cat' => 'men',  'order' => 'ASC', 'posts_per_page' => '4');
          $wc_query = new WP_Query($params);
          ?>
          <ul>
            <?php if ($wc_query->have_posts()) : ?>
              <?php while ($wc_query->have_posts()) :
                $wc_query->the_post(); ?>
                <li>
                  <h3>
                    <a href="<?php the_permalink(); ?>">
                      <?php the_title(); ?>
                    </a>
                  </h3>
                  <?php the_post_thumbnail(); ?>
                  <?php the_excerpt(); ?>
                </li>
              <?php endwhile; ?>
              <?php wp_reset_postdata(); ?>
            <?php else :  ?>
              <li>
                <?php _e('No Products'); ?>
              </li>
            <?php endif; ?>
          </ul>

        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>