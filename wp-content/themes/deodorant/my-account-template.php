<?php

/**
/* Template Name: My Account
 *
 * Displays Only about template
 
 * @package WordPress
 * @subpackage deodorant
 * @since deodorant 1.0
 */
get_header(); ?>

<?php

$post1 = get_post(29);
$post1_title = $post1->post_title;
$post1_text = $post1->post_content;
$post1_img = get_the_post_thumbnail_url($post1, 'thumbnail');




?>


<?php $image_url = wp_get_attachment_url(get_post_thumbnail_id()); ?>



<?php if (!empty(get_the_post_thumbnail())) { ?>

  <section class="page_banner" style="background-image:url('<?php echo $image_url; ?>"></section>
<?php } else { ?>
  <section class="page_banner" style="background-image:url('<?php echo esc_url(get_template_directory_uri()); ?>/images/images-about-us-1.jpg');">

  </section>
<?php } ?>

<!--********** -->


<section class="my_account_sec py-5 my-5">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-6">
        <div class="account_part">
              <?php echo do_shortcode( '[woocommerce_my_account]' ); ?>
        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer(); ?>