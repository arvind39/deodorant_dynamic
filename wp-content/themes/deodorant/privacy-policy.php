<?php
/**
/* Template Name: Privacy Policy
 *
 *
 *
 * @package WordPress
 * @subpackage deodorant
 * @since deodorant 1.0
 */
 get_header(); ?>

 <?php

$post1 = get_post(31);
$post1_title = $post1->post_title;
$post1_text = $post1->post_content;
$post1_img = get_the_post_thumbnail_url($post1, 'thumbnail'); ?>

<!--Banner Part-->

<?php
  $image_url= wp_get_attachment_url( get_post_thumbnail_id() );
  if( !empty(get_the_post_thumbnail()) ) {
?>

<section class="page_banner" style="background-image:url('<?php echo $image_url;?>"></section>
<?php } else { ?>
  <section class="page_banner" style="background-image:url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/privacy-policy-banner.jpg');">

</section>
<?php } ?>

<!--********** -->


 
<section class="term-of-services-sec1 py-5 my-4">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-md-12">
          <div class="content_box wow fadeIn">
            <h2 class="pb-2"><?php echo $post1_title ?></h2>
          </div>
        </div>
        <div class="col-md-12">
          <div class="policy_box">
            <?php echo $post1_text ?>
          </div>
        <!-- <div class="policy_box">
            <p>This deodorant Network Private Policy discloses the privacy practices associated with deodorant Group, its
              divisions, and its corporate affiliates.</p>

            <p>deodorant understands the need for your privacy and even comprehends your need to measure appropriate
              protection and manage the security of personal information.</p>


            <h4 class="pt-4">Consent</h4>
            <p>The Privacy Policy enforces the mutual agreement between you and deodorant. Therefore, please read this
              Privacy Policy details thoroughly to comprehend our significant policies and practices regarding the
              information collected and how to comply with them.</p>

            <p>By logging into our Website, you also accept to submit the information with consent to this Privacy
              Policy.
              We shall send you the notifications and offer you confirmation at different stages while collecting the
              information.</p>

            <p>If you disagree with our policies and practices, you may not be able to access or benefit from the
              specific
              portions of the Website.</p>
            <h4 class="pt-4">Scope of Our Privacy Policy </h4>
            <p>Our Privacy Policy specifies the information that gets collected by us. When you register an account with
              us, there are also chances that we can collect the information.</p>
            <h4 class="pt-4">Proprietary Rights and License </h4>
            <p>The deodorant legal statement states that all images, illustrations, designs, photographs, video clips,
              text icons, research, insights, portfolios, written information, and other materials appearing on the
              Website. </p>
            <p>With the help of email and other communications, you mutually agree to send us the required information.
              If you connect to our third parties, then also we can collect the information given to us. This Privacy
              Policy is also applicable to the personal information carried by our past, present, and prospective
              business partners.</p>
            <h4 class="pt-4">By Signing Up at Our Website, You Acknowledge To Terms and Conditions</h4>
            <p>You also agree with consent to share your personal information with deodorant following the Privacy Policy.
            </p>

            <p>You also give us the information related to servers located within and outside the United States.</p>
            <h4 class="pt-4"> Collection of Information</h4>
            <p>deodorant gathers specific personal information from users viewing the Website and business partners and
              may collect information about potential business customers or target audiences. This type of information
              gets stored by us.</p>

              <p>deodorant carries no obligation to authorize any accounts and shall retain sole discretion in your ability
              to create an Account. </p>
          </div>

        </div> -->

      </div>
    </div>
  </section> 
  <?php get_footer(); ?>