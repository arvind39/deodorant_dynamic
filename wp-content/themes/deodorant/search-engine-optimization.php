<?php

/**
/* Template Name: SEO
 *
 *
 *
 * @package WordPress
 * @subpackage deodorant
 * @since deodorant 1.0
 */
get_header(); ?>




<!--Banner Part-->

<?php
$image_url = wp_get_attachment_url(get_post_thumbnail_id());
if (!empty(get_the_post_thumbnail())) {
?>
  <section class="page_banner" style="background-image:url('<?php echo $image_url; ?>"></section>
<?php } else { ?>
  <section class="page_banner" style="background-image:url('<?php echo esc_url(get_template_directory_uri()); ?>/images/seo_banner.jpg');">

  </section>
<?php } ?>

<!--********** -->


<section id="seo" class="other-services-sec3 pt-5 my-xl-5 my-4 pb-4 px-2 px-md-0">
  <div class="container">
    <div class="col-md-12">
      <div class="default_title text-center mb-lg-5 mb-4 pb-4">
        <h2>SEO Bootcamp</h2>
        <p>Gain an edge your competition through hands-on lessons in Search Engine Optimization (SEO) and competitive
          keyword analysis</p>
      </div>
    </div>
    <div class="row align-items-center">
    <?php $posts = new WP_Query(array('post_type' => 'seo_bootcamp',   'order' => 'ASC', 'posts_per_page' => '12')); ?>

    <?php $counter = 1; ?>
    <?php while ($posts->have_posts()) : $posts->the_post(); ?>



      <?php if ($counter <= 3) { ?>
        
          <div class="col-md-4 equal_height1">
            <div class="icon_box seo_box px-xl-4 px-4 px-md-2 mb-4">
              <div class="icon mb-md-3 mb-3">
                <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="img-fluid">
              </div>
              <h4><?php echo get_the_title(); ?></h4>
              <?php echo get_the_content(); ?>
            </div>
          </div>
    

      <?php } elseif ($counter > 3 and $counter < 7) { ?>
        
          <div class="col-md-4 equal_height2">
            <div class="icon_box seo_box px-xl-4 px-4 px-md-2 mb-4">
              <div class="icon mb-md-3 mb-3">
                <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="img-fluid">
              </div>
              <h4><?php echo get_the_title(); ?></h4>
              <?php echo get_the_content(); ?>
            </div>
          </div>
     

      <?php } elseif ($counter > 6 and $counter < 10) { ?>
       
          <div class="col-md-4 equal_height3">
            <div class="icon_box seo_box px-xl-4 px-4 px-md-2 mb-4">
              <div class="icon mb-md-3 mb-3">
                <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="img-fluid">
              </div>
              <h4><?php echo get_the_title(); ?></h4>
              <?php echo get_the_content(); ?>
            </div>
          </div>
     

      <?php } elseif ($counter > 9 and $counter < 13) { ?>
        
          <div class="col-md-4 equal_height4">
            <div class="icon_box seo_box px-xl-4 px-4 px-md-2 mb-4">
              <div class="icon mb-md-3 mb-3">
                <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" class="img-fluid">
              </div>
              <h4><?php echo get_the_title(); ?></h4>
              <?php echo get_the_content(); ?>
            </div>
          </div>

      <?php }
      ?>
     
      <?php $counter++; ?>
     
    <?php endwhile; ?>
  </div>
  <!-- <div class="row align-items-center equal_height2">
        <div class="col-md-4">
          <div class="icon_box seo_box px-xl-4 px-4 px-md-2 mb-4">
            <div class="icon mb-md-3 mb-3">
              <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icons/Video Optimization.svg" class="img-fluid">
            </div>
            <h4>Video Optimization</h4>
            <ul>
              <li>Step to YouTube Video Optimization</li>
              <li>Keyword Research for Video Optimization</li>
              <li>How to Optimize YouTube Videos</li>
              <li>Factors that Affect Your Video Ranking</li>
              <li>Course Syllabus | SEO Bootcamp 2</li>
              <li>Types of Videos to Create</li>
              <li>How to Promote Your Videos</li>
            </ul>
          </div>
        </div>
        <div class="col-md-4">
          <div class="icon_box seo_box px-xl-4 px-4 px-md-2 mb-4">
            <div class="icon mb-md-3 mb-3">
              <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icons/Mobile SEO.svg" class="img-fluid">
            </div>
            <h4>Mobile SEO</h4>
            <ul>
              <li>Factors that influence Mobile SEO</li>
              <li>The importance of Mobile SEO</li>
              <li>A look at options for creating mobile friendly websites</li>
              <li>Characteristics of mobile websites</li>
              <li>Creating mobile focused content</li>
            </ul>
          </div>
        </div>

        <div class="col-md-4">
          <div class="icon_box seo_box px-xl-4 px-4 px-md-2 mb-4">
            <div class="icon mb-md-3 mb-3">
              <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icons/SEO Audit.svg" class="img-fluid">
            </div>
            <h4>SEO Audit</h4>
            <ul>
              <li>What is an SEO Audit?</li>
              <li>How to conduct an SEO Audit?</li>
              <li>Tools for conducting an SEO Audit?</li>
              <li>SEO Audit Lab</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row align-items-center equal_height3">
        <div class="col-md-4">
          <div class="icon_box seo_box px-xl-4 px-4 px-md-2 mb-4">
            <div class="icon mb-md-3 mb-3">
              <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icons/Keyword Research.svg" class="img-fluid">
            </div>
            <h4>Keyword Research</h4>
            <ul>
              <li>What is a keyword</li>
              <li>The different types of keywords</li>
              <li>Examining the intent behind a keyword</li>
              <li>Understanding the progression of a search query</li>
              <li>Steps to keyword research</li>
              <li>How to find the best keywords to target</li>
              <li>How to discover your competitors keywords</li>
              <li>Competitive keyword analysis</li>
              <li>Keyword research tools</li>
              <li>Keyword research lab</li>
            </ul>
          </div>
        </div>
        <div class="col-md-4">
          <div class="icon_box seo_box px-xl-4 px-4 px-md-2 mb-4">
            <div class="icon mb-md-3 mb-3">
              <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icons/Competitive Analysis.svg" class="img-fluid">
            </div>
            <h4>Competitive Analysis</h4>
            <ul>
              <li>How to identify your competitors?</li>
              <li>What to analyze?</li>
              <li>How to use your competitive analysis for keyword targeting?</li>
              <li>SEO Content</li>
              <li>What is SEO content</li>
              <li>Different types of SEO content</li>
              <li>Steps to creating SEO content</li>
              <li>Course Syllabus | SEO Bootcamp 1</li>
              <li>How to factor in your customer buying journey when creating SEO content</li>
              <li>How to evaluate your competitors content</li>
            </ul>
          </div>
        </div>
        <div class="col-md-4">
          <div class="icon_box seo_box px-xl-4 px-4 px-md-2 mb-4">
            <div class="icon mb-md-3 mb-3">
              <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icons/Designing for SEO.svg" class="img-fluid">
            </div>
            <h4>Designing for SEO</h4>
            <ul>
              <li>How to structure your website</li>
              <li>How to organize your web pages</li>
              <li>How to design your website navigation</li>
              <li>How to use rich snippets/structure data</li>
              <li>What are Meta robot tags</li>
              <li>How to use Meta robot tags</li>
              <li>What are 404 error pages</li>
              <li>Tips for creating 404 error pages</li>
              <li>What are 301 redirects</li>
              <li>How search engines handle 301 redirects</li>
              <li>The importance of canonicalization</li>
              <li>How to implement canonicalization</li>
              <li>How website speed influence your rankings and how to improve it</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row align-items-center equal_height4">
        <div class="col-md-4">
          <div class="icon_box seo_box px-xl-4 px-4 px-md-2 mb-4">
            <div class="icon mb-md-3 mb-3">
              <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icons/Measuring & Reporting.svg" class="img-fluid">
            </div>
            <h4>Measuring & Reporting on SEO Success</h4>
            <ul>
              <li>KPIs for Organic Search</li>
              <li>How to Report on SEO Progress</li>
              <li>Demystifying SEO Reports</li>
            </ul>
          </div>
        </div>
      </div> -->
  </div>
</section>

<?php get_footer(); ?>