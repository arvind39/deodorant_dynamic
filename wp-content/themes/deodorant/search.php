<?php

/**
/* Template Name: Search Page
 * The template for displaying search results page.

 */

get_header(); ?>


<!--Banner Part-->

<section class="page_banner default_banner" style="background-image:url('<?php echo esc_url(get_template_directory_uri()); ?>/images/images-about-us-1.jpg');">

</section>
<!---->


<section class="search_page py-5 mb-5 mt-md-5 mt-4">

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="search_result">
          <?php if (have_posts()) : ?>
            <div class="result pb-4">
              <h3><?php _e('Search Results for: ', 'your-theme');
                  ?><b><?php the_search_query(); ?></b></h3>
            </div>


            <ul class="products">
              <?php while (have_posts()) : the_post() ?>
                <li>
                  <?php wc_get_template_part('content', 'product'); ?>
                </li>

              <?php

              endwhile; ?>

            </ul>

          <?php else :                ?>

            <div id="post-0" class="post no-results not-found">

              <h3 class="entry-title"><?php _e('Nothing Found', 'your-theme') ?></h3>

              <div class="entry-content">

                <p><?php _e('Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'your-theme'); ?></p>

                <!-- <?php //get_search_form(); 
                      ?> -->
                <div class="search_form_page">
                  <form id="labnol" method="get" action="<?php echo esc_url(home_url('/')); ?>">
                    <div class="speech">
                      <img class="" id="start_img2" onclick="startDictation2()" src="data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABQAAD/4QMqaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjUtYzAxNCA3OS4xNTE0ODEsIDIwMTMvMDMvMTMtMTI6MDk6MTUgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MkFEODdGMUQyMDEzMTFFQ0IzNzhCQUI3OTg0N0QzMzkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MkFEODdGMUUyMDEzMTFFQ0IzNzhCQUI3OTg0N0QzMzkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoyQUQ4N0YxQjIwMTMxMUVDQjM3OEJBQjc5ODQ3RDMzOSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoyQUQ4N0YxQzIwMTMxMUVDQjM3OEJBQjc5ODQ3RDMzOSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pv/uAA5BZG9iZQBkwAAAAAH/2wCEAAICAgICAgICAgIDAgICAwQDAgIDBAUEBAQEBAUGBQUFBQUFBgYHBwgHBwYJCQoKCQkMDAwMDAwMDAwMDAwMDAwBAwMDBQQFCQYGCQ0LCQsNDw4ODg4PDwwMDAwMDw8MDAwMDAwPDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIADIAJgMBEQACEQEDEQH/xAGiAAAABwEBAQEBAAAAAAAAAAAEBQMCBgEABwgJCgsBAAICAwEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAgEDAwIEAgYHAwQCBgJzAQIDEQQABSESMUFRBhNhInGBFDKRoQcVsUIjwVLR4TMWYvAkcoLxJUM0U5KismNzwjVEJ5OjszYXVGR0w9LiCCaDCQoYGYSURUaktFbTVSga8uPzxNTk9GV1hZWltcXV5fVmdoaWprbG1ub2N0dXZ3eHl6e3x9fn9zhIWGh4iJiouMjY6PgpOUlZaXmJmam5ydnp+So6SlpqeoqaqrrK2ur6EQACAgECAwUFBAUGBAgDA20BAAIRAwQhEjFBBVETYSIGcYGRMqGx8BTB0eEjQhVSYnLxMyQ0Q4IWklMlomOywgdz0jXiRIMXVJMICQoYGSY2RRonZHRVN/Kjs8MoKdPj84SUpLTE1OT0ZXWFlaW1xdXl9UZWZnaGlqa2xtbm9kdXZ3eHl6e3x9fn9zhIWGh4iJiouMjY6Pg5SVlpeYmZqbnJ2en5KjpKWmp6ipqqusra6vr/2gAMAwEAAhEDEQA/APv5ir8jP+fnv/OcXm3/AJx10/y5+Vn5TXkemfmJ5ztJNQ1fzKUWSXStMDeknoI4IEs7cgrkHiFJHxUIVfgr5W/5zD/5yl8teZIfNunfnz5vfUoZvXkGo6tcXtrMa1KTWtw7xSKf5WWmKv6of+cJP+cmR/zlR+Ruj/mBfWUOmebNOuJNG86afbV9BdQtgCZYgakJMjLIF/Z5U7Yq+vMVdir8gv8AnO7yL5F8yfnJpV/5l8ueUdUvU8t2sYutdt9MkufTWeYhFa885aA/EFjT/RmH+WegVfFTflV+VDfD/gr8uOA+yps/L9adqkfmcMVfrL/zgH5c8ueWvyx82WnlrSdC0i0n8yPLPBoMVnFA0n1aFeTiy1/zChagA3nQ0p+7/aKr7txV+av/ADnZ/wA5g/mJ/wA40efv+ce/K/kjR9Kv7D8yNXkj8zy6kju7W0dxbW4hgKkcCfXLF96UG2Kvl3/n41/zk3oH5RfnloflvVPKWu6xLc+U7K/jvNO1iOxi4zT3C8DE1vMSy8NzXvir4D/6Hw8mf+W881/+FJF/2RYq/Tn/AJxr/wCcrZk/5wn/AOciPzx8l+Vbqx1P8ub66fTdN16/F+k9wtralXLxxQkIvq/ZpuR13xVmGj/85u/mrqn/AD7g81/85TNpejQ/mf5cnj06NfRc6dNLJrNnp31kwcqikV0Tw5U5L1pirHf+fwP5O+YvNn5S+RPzk8pwTXOpfkvq0s+rJAvJ4tPvzD/pRHhBNBHX2evQYq8F/N/8rtI/5+c/kF+W/wCb35Maxp8H58fllpS6N558jXk6xyXKqierEzkclYOvqQORxYMyk13Cr81/Lv8Az7r/AOcx/MXmK38uj8k9Y0N5phFLrOrGK10+EfzyXHNhx91DfLFX3x/zk3P5O/5w3/5xC0j/AJwm8leYYvOv50/mbdx3X5jNp3xmMXMqtKjRrUr6jKkMKH4iq8mHiq+57f8A5xI802v/AD7Iv/8AnHOGMr+YGpeVhrV3ZcfjbV11CPXRZU/n5RLb/wCsMVfptqemafrWnX2katZQ6jpepwSWuoWFwgeKaGVSjxup2IZSQRir8Q/zc/59efmh+W3ny7/Nb/nCP8zpPI95PI0x8k3d3LZtByYkw212iyRzRVPwxXCUWn2mxV8leTP+cgP+flH53/mHrn/OOXl38044fPWgG7t/MkoTTrFolsm9O4LXscBrQ94lqe2Kv0g/5xG/59l6R+UPm2H85Pz281D82fzfWc3tlyMs2n2F0ety01z+9u5h+y7qoX+UmjYq/V/FXYq+ef8AnIv/AJye/Kj/AJxg8saZ5m/NDVLi2j1u8+paHpVhA91eXUqqXcpEnRUUVZjQDYdSMVfzjf8AOMf/ADlb+WH5Tf8AOZ35mfnt5ph1keSPNsmtNpRt7Qy3QGoTc4vUiFKbdaYq/qG/LX8x/KH5t+R/Ln5i+Q9VXWfKnmm0W80m+CtGxU7MkkbgMjowKurCqsCDirOcVdir5i/5yP8A7ryt/wCSW+3c/wDk3/sdE/453v8A78+jFXy1/wC4UYq+8fya/wDJdaD/AMoT/u//AMl3/wAo7/fv/vH/AMb/AOXyxV6jir//2Q==" />
                      <input type="text" class="search-query form-control" value="<?php echo get_search_query(); ?>" name="s" id="transcript" />
                    </div>
                  </form>
                </div>
              </div>

            </div>


          <?php





          endif;
          ?>








        </div>


      </div>

    </div>

  </div>

</section>

<?php get_footer(); ?>

<script>
  function startDictation2() {
    if (window.hasOwnProperty('webkitSpeechRecognition')) {

      var recognition = new webkitSpeechRecognition();

      recognition.continuous = false;
      recognition.interimResults = false;

      recognition.lang = "en-US";
      recognition.start();


      start_img2.src = 'data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2024%2024%22%3E%3Cpath%20fill%3D%22none%22%20d%3D%22M0%200h24v24H0V0z%22%2F%3E%3Cpath%20d%3D%22M12%206v3l4-4-4-4v3c-4.42%200-8%203.58-8%208%200%201.57.46%203.03%201.24%204.26L6.7%2014.8c-.45-.83-.7-1.79-.7-2.8%200-3.31%202.69-6%206-6zm6.76%201.74L17.3%209.2c.44.84.7%201.79.7%202.8%200%203.31-2.69%206-6%206v-3l-4%204%204%204v-3c4.42%200%208-3.58%208-8%200-1.57-.46-3.03-1.24-4.26z%22%2F%3E%3C%2Fsvg%3E';


      recognition.onstart = function() {

        start_img2.src = 'data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2024%2024%22%3E%3Cpath%20fill%3D%22none%22%20d%3D%22M0%200h24v24H0V0z%22%2F%3E%3Cpath%20d%3D%22M9%2013c2.21%200%204-1.79%204-4s-1.79-4-4-4-4%201.79-4%204%201.79%204%204%204zm0-6c1.1%200%202%20.9%202%202s-.9%202-2%202-2-.9-2-2%20.9-2%202-2zm0%208c-2.67%200-8%201.34-8%204v2h16v-2c0-2.66-5.33-4-8-4zm-6%204c.22-.72%203.31-2%206-2%202.7%200%205.8%201.29%206%202H3zM15.08%207.05c.84%201.18.84%202.71%200%203.89l1.68%201.69c2.02-2.02%202.02-5.07%200-7.27l-1.68%201.69zM20.07%202l-1.63%201.63c2.77%203.02%202.77%207.56%200%2010.74L20.07%2016c3.9-3.89%203.91-9.95%200-14z%22%2F%3E%3C%2Fsvg%3E';
      };
      recognition.onend = function() {

        start_img2.src = 'data:data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABQAAD/4QMqaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjUtYzAxNCA3OS4xNTE0ODEsIDIwMTMvMDMvMTMtMTI6MDk6MTUgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MkFEODdGMUQyMDEzMTFFQ0IzNzhCQUI3OTg0N0QzMzkiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MkFEODdGMUUyMDEzMTFFQ0IzNzhCQUI3OTg0N0QzMzkiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoyQUQ4N0YxQjIwMTMxMUVDQjM3OEJBQjc5ODQ3RDMzOSIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoyQUQ4N0YxQzIwMTMxMUVDQjM3OEJBQjc5ODQ3RDMzOSIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pv/uAA5BZG9iZQBkwAAAAAH/2wCEAAICAgICAgICAgIDAgICAwQDAgIDBAUEBAQEBAUGBQUFBQUFBgYHBwgHBwYJCQoKCQkMDAwMDAwMDAwMDAwMDAwBAwMDBQQFCQYGCQ0LCQsNDw4ODg4PDwwMDAwMDw8MDAwMDAwPDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIADIAJgMBEQACEQEDEQH/xAGiAAAABwEBAQEBAAAAAAAAAAAEBQMCBgEABwgJCgsBAAICAwEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAgEDAwIEAgYHAwQCBgJzAQIDEQQABSESMUFRBhNhInGBFDKRoQcVsUIjwVLR4TMWYvAkcoLxJUM0U5KismNzwjVEJ5OjszYXVGR0w9LiCCaDCQoYGYSURUaktFbTVSga8uPzxNTk9GV1hZWltcXV5fVmdoaWprbG1ub2N0dXZ3eHl6e3x9fn9zhIWGh4iJiouMjY6PgpOUlZaXmJmam5ydnp+So6SlpqeoqaqrrK2ur6EQACAgECAwUFBAUGBAgDA20BAAIRAwQhEjFBBVETYSIGcYGRMqGx8BTB0eEjQhVSYnLxMyQ0Q4IWklMlomOywgdz0jXiRIMXVJMICQoYGSY2RRonZHRVN/Kjs8MoKdPj84SUpLTE1OT0ZXWFlaW1xdXl9UZWZnaGlqa2xtbm9kdXZ3eHl6e3x9fn9zhIWGh4iJiouMjY6Pg5SVlpeYmZqbnJ2en5KjpKWmp6ipqqusra6vr/2gAMAwEAAhEDEQA/APv5ir8jP+fnv/OcXm3/AJx10/y5+Vn5TXkemfmJ5ztJNQ1fzKUWSXStMDeknoI4IEs7cgrkHiFJHxUIVfgr5W/5zD/5yl8teZIfNunfnz5vfUoZvXkGo6tcXtrMa1KTWtw7xSKf5WWmKv6of+cJP+cmR/zlR+Ruj/mBfWUOmebNOuJNG86afbV9BdQtgCZYgakJMjLIF/Z5U7Yq+vMVdir8gv8AnO7yL5F8yfnJpV/5l8ueUdUvU8t2sYutdt9MkufTWeYhFa885aA/EFjT/RmH+WegVfFTflV+VDfD/gr8uOA+yps/L9adqkfmcMVfrL/zgH5c8ueWvyx82WnlrSdC0i0n8yPLPBoMVnFA0n1aFeTiy1/zChagA3nQ0p+7/aKr7txV+av/ADnZ/wA5g/mJ/wA40efv+ce/K/kjR9Kv7D8yNXkj8zy6kju7W0dxbW4hgKkcCfXLF96UG2Kvl3/n41/zk3oH5RfnloflvVPKWu6xLc+U7K/jvNO1iOxi4zT3C8DE1vMSy8NzXvir4D/6Hw8mf+W881/+FJF/2RYq/Tn/AJxr/wCcrZk/5wn/AOciPzx8l+Vbqx1P8ub66fTdN16/F+k9wtralXLxxQkIvq/ZpuR13xVmGj/85u/mrqn/AD7g81/85TNpejQ/mf5cnj06NfRc6dNLJrNnp31kwcqikV0Tw5U5L1pirHf+fwP5O+YvNn5S+RPzk8pwTXOpfkvq0s+rJAvJ4tPvzD/pRHhBNBHX2evQYq8F/N/8rtI/5+c/kF+W/wCb35Maxp8H58fllpS6N558jXk6xyXKqierEzkclYOvqQORxYMyk13Cr81/Lv8Az7r/AOcx/MXmK38uj8k9Y0N5phFLrOrGK10+EfzyXHNhx91DfLFX3x/zk3P5O/5w3/5xC0j/AJwm8leYYvOv50/mbdx3X5jNp3xmMXMqtKjRrUr6jKkMKH4iq8mHiq+57f8A5xI802v/AD7Iv/8AnHOGMr+YGpeVhrV3ZcfjbV11CPXRZU/n5RLb/wCsMVfptqemafrWnX2katZQ6jpepwSWuoWFwgeKaGVSjxup2IZSQRir8Q/zc/59efmh+W3ny7/Nb/nCP8zpPI95PI0x8k3d3LZtByYkw212iyRzRVPwxXCUWn2mxV8leTP+cgP+flH53/mHrn/OOXl38044fPWgG7t/MkoTTrFolsm9O4LXscBrQ94lqe2Kv0g/5xG/59l6R+UPm2H85Pz281D82fzfWc3tlyMs2n2F0ety01z+9u5h+y7qoX+UmjYq/V/FXYq+ef8AnIv/AJye/Kj/AJxg8saZ5m/NDVLi2j1u8+paHpVhA91eXUqqXcpEnRUUVZjQDYdSMVfzjf8AOMf/ADlb+WH5Tf8AOZ35mfnt5ph1keSPNsmtNpRt7Qy3QGoTc4vUiFKbdaYq/qG/LX8x/KH5t+R/Ln5i+Q9VXWfKnmm0W80m+CtGxU7MkkbgMjowKurCqsCDirOcVdir5i/5yP8A7ryt/wCSW+3c/wDk3/sdE/453v8A78+jFXy1/wC4UYq+8fya/wDJdaD/AMoT/u//AMl3/wAo7/fv/vH/AMb/AOXyxV6jir//2Q=='

      };

      recognition.onresult = function(e) {
        document.getElementById('transcript').value = e.results[0][0].transcript;
        recognition.stop();

        document.getElementById('labnol').submit();

      };

      recognition.onerror = function(e) {
        recognition.stop();
        start_img2.src = 'data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2024%2024%22%3E%3Cpath%20fill%3D%22none%22%20d%3D%22M0%200h24v24H0V0z%22%2F%3E%3Cpath%20d%3D%22M12%206v3l4-4-4-4v3c-4.42%200-8%203.58-8%208%200%201.57.46%203.03%201.24%204.26L6.7%2014.8c-.45-.83-.7-1.79-.7-2.8%200-3.31%202.69-6%206-6zm6.76%201.74L17.3%209.2c.44.84.7%201.79.7%202.8%200%203.31-2.69%206-6%206v-3l-4%204%204%204v-3c4.42%200%208-3.58%208-8%200-1.57-.46-3.03-1.24-4.26z%22%2F%3E%3C%2Fsvg%3E';
      }

    }
  }
</script>