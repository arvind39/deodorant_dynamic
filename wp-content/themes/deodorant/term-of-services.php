<?php
/**
/* Template Name: Term of Use
 *
 *
 *
 * @package WordPress
 * @subpackage deodorant
 * @since deodorant 1.0
 */
 get_header(); ?>
<?php

$post1 = get_post(34);
$post1_title = $post1->post_title;
$post1_text = $post1->post_content;
$post1_img = get_the_post_thumbnail_url($post1, 'thumbnail'); ?>

<!--Banner Part-->

<?php
  $image_url= wp_get_attachment_url( get_post_thumbnail_id() );
  if( !empty(get_the_post_thumbnail()) ) {
?>

<section class="page_banner" style="background-image:url('<?php echo $image_url;?>"></section>
<?php } else { ?>
  <section class="page_banner" style="background-image:url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/privacy-policy-banner.jpg');">

</section>
<?php } ?>

<!--********** -->
  <section class="term-of-services-sec1 py-5 my-4">
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-md-12">
          <div class="content_box wow fadeIn">
            <h2 class="pb-2"><?php echo $post1_title ?></h2>
          </div>
        </div>
        <div class="col-md-12">
          <div class="policy_box">
          <?php echo $post1_text ?>
          </div>

        </div>

      </div>
    </div>
  </section>
  <?php get_footer(); ?>